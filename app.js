let forms = document.querySelectorAll('form');
forms.forEach(form => {
    form.addEventListener('submit', e => {
        let form = new FormData(e.target);
        callToMe(form);
        e.preventDefault();
    });
})

let tels = document.querySelectorAll('[type=tel]');
tels.forEach(input => {
    input.onkeypress = function (event) {
        input.style.borderColor = "#B4B4B4";
        let number = input.value.replace(/\D/g, "");
        number = number.replace(/^0/, "");
        if (number.length > 10) {
            number = number.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
        } else if (number.length > 5) {
            number = number.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
        } else if (number.length > 2) {
            number = number.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
        } else {
            number = number.replace(/^(\d*)/, "($1");
        }
        input.value = number;
    }

    input.onblur = function (event) {
        let number = input.value.replace(/\D/g, "");
        number = number.replace(/^0/, "");
        if (number.length > 10) {
            number = number.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
        } else if (number.length > 5) {
            number = number.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
        } else if (number.length > 2) {
            number = number.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
        } else if (number.length > 0) {
            number = number.replace(/^(\d*)/, "($1");
        }
        input.value = number;
    }
})

const parseNumber = number => {
    number = number.replace(/[^0-9]/gi, '');
    if (number.length == 10 || number.length == 11) {
        number = 55 + number;
    }
    return number;
}

const callToMe = (form) => {
    let params = {
        consumer_number: parseNumber(form.get('consumer_number')), // Número do consumidor
        advert_uuid: form.get('advert_uuid'), // Identificador unico do anúncio
        first_call: form.has('first_call') ? form.get('first_call') : 'agent' // Onde tocar primeiro
    }

    fetch(new Request('https://blitz.phonetrack.com.br/makecall', {
        method: 'POST',
        body: JSON.stringify(params)
    }))
    .then(response => response.json())
    .then(data => {
        if (data.call_id === undefined) {
            alert("Ocorreu um erro, por favor tente novamente.");
            return;
        }
    });
}