let targets = document.querySelectorAll('[data-modal]');
targets.forEach(target => {
    target.addEventListener('click', e => {
        let idModal = e.target.getAttribute('data-modal');
        let modal = document.querySelector(idModal);
        modal.style.display = 'block';
        closeModal(modal)
    });
})

let closeModal = modal => {
    let close = modal.querySelector('.modal-close');
    close.addEventListener('click', e => {
        modal.style.display = 'none';
    });
}